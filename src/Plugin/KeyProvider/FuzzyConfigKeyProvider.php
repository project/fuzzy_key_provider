<?php

namespace Drupal\fuzzy_key_provider\Plugin\KeyProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\key\Exception\KeyValueNotSetException;
use Drupal\key\Plugin\KeyProviderBase;
use Drupal\key\Plugin\KeyPluginFormInterface;
use Drupal\key\Plugin\KeyProviderSettableValueInterface;
use Drupal\key\KeyInterface;

/**
 * Adds a key provider that allows a key to be stored in configuration.
 *
 * @KeyProvider(
 *   id = "fuzzy_config",
 *   label = @Translation("Fuzzy configuration"),
 *   description = @Translation("The Configuration key provider stores the key
 *   in Drupal's configuration system with a bit of fuzzy."), storage_method =
 *   "config", key_value = {
 *     "accepted" = TRUE,
 *     "required" = FALSE
 *   }
 * )
 */
class FuzzyConfigKeyProvider extends KeyProviderBase implements KeyPluginFormInterface, KeyProviderSettableValueInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key) {
    $key_value = isset($this->configuration['key_value']) ? $this->decrypt($this->configuration['key_value']) : '';

    return $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyValue(KeyInterface $key, $key_value) {
    $this->configuration['key_value'] = $this->encrypt($key_value);

    if (isset($this->configuration['key_value'])) {
      return TRUE;
    }
    else {
      throw new KeyValueNotSetException();
    }
  }

  /**
   * Encrypt a string.
   *
   * @param string $data
   *   Data to protect.
   *
   * @return string
   *   The encrypted data.
   *
   * @see https://bhoover.com/using-php-openssl_encrypt-openssl_decrypt-encrypt-decrypt-data/
   */
  protected function encrypt($data) {
    $key = Settings::get('hash_salt');
    // Remove the base64 encoding from our key.
    $encryption_key = base64_decode($key);
    // Generate an initialization vector.
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    // Encrypt the data using AES 256 encryption in CBC mode using our
    // encryption key and initialization vector.
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    // The $iv is just as important as the key for decrypting, so save it with
    // our encrypted data using a unique separator (::).
    return base64_encode($encrypted . '::' . $iv);
  }

  /**
   * Decrypt a string.
   *
   * @param string $data
   *   The encrypted data.
   *
   * @return string
   *   Data protected.
   *
   * @see https://bhoover.com/using-php-openssl_encrypt-openssl_decrypt-encrypt-decrypt-data/
   */
  protected function decrypt($data) {
    $key = Settings::get('hash_salt');
    // Remove the base64 encoding from our key.
    $encryption_key = base64_decode($key);
    // To decrypt, split the encrypted data from our IV - our unique separator
    // used was "::".
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteKeyValue(KeyInterface $key) {
    // Nothing needs to be done, since the value will have been deleted
    // with the Key entity.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function obscureKeyValue($key_value, array $options = []) {
    // Key values are not obscured when this provider is used.
    return $key_value;
  }

}
